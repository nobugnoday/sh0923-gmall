package com.atguigu.gmall.payment.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.atguigu.gmall.common.exception.PaymentException;
import com.atguigu.gmall.oms.entity.OrderEntity;
import com.atguigu.gmall.payment.config.AlipayTemplate;
import com.atguigu.gmall.payment.interceptor.LoginInterceptor;
import com.atguigu.gmall.payment.pojo.PaymentInfoEntity;
import com.atguigu.gmall.payment.pojo.UserInfo;
import com.atguigu.gmall.payment.service.PaymentService;
import com.atguigu.gmall.payment.vo.PayAsyncVo;
import com.atguigu.gmall.payment.vo.PayVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;

@Controller
public class PayController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AlipayTemplate alipayTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("pay.html")
    public String paySelect(@RequestParam("orderToken")String ordetToken, Model model){

        OrderEntity orderEntity = this.paymentService.queryOrderByToken(ordetToken);
        if (orderEntity == null){
            throw new PaymentException("您查找的订单不存在 请重新输入。。。。。。。。。。。。。");
        }
        // 获取当前用户的登录信息
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        // 判断订单的用户是否是当前的用户
        if (orderEntity.getUserId() != userInfo.getUserId()){
            throw new PaymentException("亲 这个订单不输入您 。。。。。。。。。。");
        }
        // 判断订单状态是否代付款的状态
        if (orderEntity.getStatus() != 0){
            throw new PaymentException("亲 这个订单已经失效 请重新输入");
        }

        model.addAttribute("orderEntity",orderEntity);
        return "pay";
    }


    @GetMapping("alipay.html")
    @ResponseBody
    public String toAlipay(@RequestParam("orderToken")String ordetToken){

        OrderEntity orderEntity = this.paymentService.queryOrderByToken(ordetToken);
        if (orderEntity == null){
            throw new PaymentException("您查找的订单不存在 请重新输入。。。。。。。。。。。。。");
        }
        // 获取当前用户的登录信息
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        // 判断订单的用户是否是当前的用户
        if (orderEntity.getUserId() != userInfo.getUserId()){
            throw new PaymentException("亲 这个订单不输入您 。。。。。。。。。。");
        }
        // 判断订单状态是否代付款的状态
        if (orderEntity.getStatus() != 0){
            throw new PaymentException("亲 这个订单已经失效 请重新输入");
        }


        // 调用支付宝的支付接口
        try {
            PayVo payVo = new PayVo();
            payVo.setOut_trade_no(ordetToken);
            // 注意：一定要写成0.01
            payVo.setTotal_amount("0.01");
            payVo.setSubject("谷粒商城支付平台");
            // 生成对账记录
            Long payId = this.paymentService.savePaymentInfo(payVo);
            // 为了方便将来校验业务参数，这里把对账表的id放入该参数
            payVo.setPassback_params(payId.toString());
            String form = this.alipayTemplate.pay(payVo);
            return form;
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }
//        // 调用支付宝 的支持接口
//        try {
//            PayVo payVo = new PayVo();
//            payVo.setOut_trade_no(ordetToken);  // 商户订单号
////            payVo.setTotal_amount(orderEntity.getPayAmount().toString());      // 付款金额，必填
//            payVo.setTotal_amount("0.0.1");      // 付款金额，必填
//            payVo.setSubject("谷粒商城支付平台");   // 订单名称 必填
//            // 为了方便将来校验业务参数 ， 这里把对账表的id放入该参数
//
//            // 生成记账记录
////            Long payId = this.paymentService.savePaymentInfo(payVo);
////            payVo.setPassback_params(payId.toString());    // 支付宝会在异步通知时将该参数原样返回。本参数必须进行UrlEncode之后才可以发送给支付宝。可选
//
//            String form = this.alipayTemplate.pay(payVo);
//            return form;
//        } catch (AlipayApiException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @GetMapping("pay/ok")
    public String payOk(){

        return "paysuccess";
    }

    @PostMapping("pay/success")
    @ResponseBody
    public Object PaySuccess(PayAsyncVo payAsyncVo){

        // 1 验签 ：确定是否是支付宝发送的  failure
        Boolean flag = this.alipayTemplate.checkSignature(payAsyncVo);
        if (!flag){
            return "failure";
        }

        // 2 校验业务参数  通知中的 app_id、out_trade_no、total_amount
        String app_id = payAsyncVo.getApp_id();
        String out_trade_no = payAsyncVo.getOut_trade_no();
        String total_amount = payAsyncVo.getTotal_amount();
        
        // 获取请求回调参数 ， 查询对账记录
        String payId = payAsyncVo.getPassback_params();
        PaymentInfoEntity paymentInfoEntity = this.paymentService.queryPaymentIdFoById(payId);

        if (!StringUtils.equals(app_id,this.alipayTemplate.getApp_id())|| !StringUtils.equals(out_trade_no,paymentInfoEntity.getOutTradeNo()) || new BigDecimal(total_amount).compareTo(paymentInfoEntity.getTotalAmount()) != 0){
            return "failure";
        }
        // 3  校验支付状态
        if (!StringUtils.equals("TRADE_SUCCESS",payAsyncVo.getTrade_status())){
            return "failure";
        }


        // 4 更新对账表
        paymentInfoEntity.setTradeNo(payAsyncVo.getTrade_no());
        paymentInfoEntity.setPaymentStatus(1);
        paymentInfoEntity.setCallbackTime(new Date());
        paymentInfoEntity.setCallbackContent(JSON.toJSONString(payAsyncVo));
        if (this.paymentService.updateStatus(paymentInfoEntity) != 1) {
            return "failure";
        }
        // 5 跟新订单状态，并减库存
        this.rabbitTemplate.convertAndSend("ORDER_EXCHANGE","order.pay",out_trade_no);


        // 6 返回结果给支付宝
        

        return "hello paysccess";
    }


}

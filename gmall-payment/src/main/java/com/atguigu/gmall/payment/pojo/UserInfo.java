package com.atguigu.gmall.payment.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserInfo {

    private Long userId;
    private String userKey;
}

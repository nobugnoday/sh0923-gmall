package com.atguigu.gmall.pms.vo;

import lombok.Data;

@Data
public class AttrValuesVo {
    private Long attrId;
    private String attrName;
    private String attrValue;
}

package com.atguigu.gmall.pms.api;

import com.atguigu.gmall.common.bean.PageParamVo;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.pms.vo.ItemGroupVo;
import com.atguigu.gmall.pms.vo.SaleAttrValuesVo;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

public interface GmallPmsApi {

    @PostMapping("/pms/spu/json")
    public ResponseVo<List<SpuEntity>> querySpuPage(@RequestBody PageParamVo paramVo);

    // 根据id 查询sku 数据信息
    @GetMapping("pms/sku/spu/{spuId}")
    public ResponseVo<List<SkuEntity>> querySkuBySpuId(@PathVariable("spuId")Long spuId);

    // 根据skuId 查询sku
    @GetMapping("pms/sku/{id}")
    public ResponseVo<SkuEntity> querySkuById(@PathVariable("id") Long id);

     // 根据品牌id查询品牌
    @GetMapping("pms/brand/{id}")
    public ResponseVo<BrandEntity> queryBrandById(@PathVariable("id") Long id);

   // 根据skuId 查询spu
    @GetMapping("pms/spu/{id}")
    public ResponseVo<SpuEntity> querySpuById(@PathVariable("id") Long id);

    @GetMapping("pms/category/{id}")
    public ResponseVo<CategoryEntity> queryCategoryById(@PathVariable("id") Long id);

    // 根据三级分类的id 查询一二三级 分类
    @GetMapping("pms/category/cid/{cid}")
    public ResponseVo<List<CategoryEntity>> queryLvlAllCategoriesByCid3(@PathVariable("cid") Long cid);

    @GetMapping("pms/category/parent/{parentId}")
    public ResponseVo<List<CategoryEntity>> queryCategoriesById(@PathVariable("parentId") Long pid);

    @GetMapping("pms/category/cates/{pid}")
    public ResponseVo<List<CategoryEntity>> queryLv2CatesWithSubsByPid(@PathVariable("pid") Long pid);

    @GetMapping("pms/skuattrvalue/category/{cid}")
    public ResponseVo<List<SkuAttrValueEntity>> querySearchAttrValueBySkuId(
            @PathVariable("cid") Long cid,
            @RequestParam("skuId") Long skuId
    );

    @GetMapping("pms/spuattrvalue/category/{cid}")
    public ResponseVo<List<SpuAttrValueEntity>> querySearchAttrValuesBySpuId(
            @PathVariable("cid") Long cid,
            @RequestParam("spuId") Long spuId
    );


    @GetMapping("pms/spudesc/{spuId}")
    public ResponseVo<SpuDescEntity> querySpuDescById(@PathVariable("spuId") Long spuId);

    // 根据skuId 查询当前sku的销售属性      V
    @GetMapping("pms/skuattrvalue/spu/{spuId}")
    public ResponseVo<List<SaleAttrValuesVo>> querySalesAttrsBySpuId(@PathVariable("spuId")Long spuId);

    // 根据skuId 查询当前sku的销售属性      V
    @GetMapping("pms/skuattrvalue/sku/{skuId}")
    public ResponseVo<List<SkuAttrValueEntity>> querySaleAttrValueBySkuId(@PathVariable("skuId")Long skuId);

    // 根据skuId 查询spu下所有销售属性的skuId 的映射关系   V
    @GetMapping("pms/skuattrvalue/mapping/{spuId}")
    public ResponseVo<Map<String ,Object>> querySaleAttrsMappingSkuIdBySpuId(@PathVariable("spuId")Long spuId);

    //    根据skuId 查询sku的图片列表      V
    @GetMapping("pms/skuimages/sku/{skuId}")
    public ResponseVo<List<SkuImagesEntity>> queryImageBySkuId(@PathVariable("skuId")Long skuId);

    // 根据分类Id，spuId ,skuId 查询分组及组下规格参数 和值  V
    @GetMapping("pms/attrgroup/group/withAttrValues/{cid}")
    public ResponseVo<List<ItemGroupVo>> queryGroupWithAttrValuesByCidAndSpuIdAndSkuId(
            @PathVariable("cid")Long cid,
            @RequestParam("spuId")Long spuId,
            @RequestParam("skuId")Long skuId
    );

}

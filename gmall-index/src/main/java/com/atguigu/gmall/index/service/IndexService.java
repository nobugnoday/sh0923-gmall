package com.atguigu.gmall.index.service;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.index.aspect.GmallCache;
import com.atguigu.gmall.index.feign.GmallPmsClient;
import com.atguigu.gmall.index.utils.DistributedLock;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RCountDownLatch;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class IndexService {

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private DistributedLock distributedLock;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private static final String KEY_PREFIX = "index:cates:";

    private static final String LOCK_PREFIX = "index:cates:lock";
    
    // 查询一级分类
    public List<CategoryEntity> queryLvlCategories() {
        ResponseVo<List<CategoryEntity>> listResponseVo = this.pmsClient.queryCategoriesById(1l);
        return listResponseVo.getData();
    }

    // 根据父类的id  查询二级 三级分类的id
    @GmallCache(prefix = KEY_PREFIX,timeout = 259200,random =7000,lock = LOCK_PREFIX )
    public List<CategoryEntity> queryLv2WithSubByPid(Long pid) {
        ResponseVo<List<CategoryEntity>> listResponseVo = this.pmsClient.queryLv2CatesWithSubsByPid(pid);
        List<CategoryEntity> categoryEntities = listResponseVo.getData();
        return categoryEntities;
    }


    // 根据父类的id  查询二级 三级分类的id
    public List<CategoryEntity> queryLv2WithSubByPid2(Long pid) {
        // 1. 查询缓存 ， 缓存命中的直接返回
        String json = this.redisTemplate.opsForValue().get(KEY_PREFIX + pid);
        if (StringUtils.isNotBlank(json)){
            return JSON.parseArray(json,CategoryEntity.class);
        }

        // 为了防止缓存的击穿
        RLock fairLock = this.redissonClient.getFairLock(LOCK_PREFIX + pid);
        fairLock.lock();

        try {
            // 在获取分布式锁的过程中，可能有其他的请求已经把数据放入缓存，此时应该再次确认缓中是否存在
            String json2 = this.redisTemplate.opsForValue().get(KEY_PREFIX + pid);
            if (StringUtils.isNotBlank(json2)){
                return JSON.parseArray(json2,CategoryEntity.class);
            }

            ResponseVo<List<CategoryEntity>> listResponseVo = this.pmsClient.queryLv2CatesWithSubsByPid(pid);
            List<CategoryEntity> categoryEntities = listResponseVo.getData();
            if (CollectionUtils.isEmpty(categoryEntities)) {
                // 为了防止缓存穿透 ，数据即使为空也缓存，只是缓存时间较短
                this.redisTemplate.opsForValue().set(KEY_PREFIX + pid,JSON.toJSONString(categoryEntities),5, TimeUnit.MINUTES);
            } else {
                // 为了防止缓存的雪崩 ， 给缓存时间添加随机值
                this.redisTemplate.opsForValue().set(KEY_PREFIX + pid,JSON.toJSONString(categoryEntities),180l + new Random().nextInt(), TimeUnit.DAYS);
            }
            return categoryEntities;
        } finally {
            fairLock.unlock();
        }
    }

    public void testLock() {
        // 获取锁
        RLock lock = redissonClient.getLock("lock");
        lock.lock();

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String numString = this.redisTemplate.opsForValue().get("num");
            // 判断是否为空
            if (StringUtils.isBlank(numString)) {
                return;
            }
            int num = Integer.parseInt(numString);
            this.redisTemplate.opsForValue().set("num",String.valueOf(++num));
            lock.unlock();
    }

    public void testLock3() {
             //获取锁
            String uuid = UUID.randomUUID().toString();
            Boolean lock = this.distributedLock.tryLock("lock", uuid, 30);
            if (lock) {

                String numString = this.redisTemplate.opsForValue().get("num");
                // 判断是否为空
                if (StringUtils.isBlank(numString)) {
                    return;
                }
                int num = Integer.parseInt(numString);
                this.redisTemplate.opsForValue().set("num",String.valueOf(++num));

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 释放锁
                this.distributedLock.unlock("lock",uuid);
            }
    }


    public void testSubLock(String uuid) {
        //获取锁
        Boolean lock = this.distributedLock.tryLock("lock", uuid, 30);

        System.out.println("测试可重入锁");
        // 释放锁
        this.distributedLock.unlock("lock",uuid);
    }


    public void testLock2() {

        // 获取锁   为了防止死锁设置过期时间
        String  uuid = UUID.randomUUID().toString();
        Boolean flag = this.redisTemplate.opsForValue().setIfAbsent("lock", uuid,3,TimeUnit.SECONDS);
        if (!flag){
            // 重试
            try {
                Thread.sleep(80);
                this.testLock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else {
            // 设置过期时间  不具有原子性
            //  this.redisTemplate.expire("lock",3,TimeUnit.SECONDS);
            String numString = this.redisTemplate.opsForValue().get("num");
            // 判断是否为空
            if (StringUtils.isBlank(numString)) {
                return;
            }

            int num = Integer.parseInt(numString);
            this.redisTemplate.opsForValue().set("num",String.valueOf(++num));
            // 释放锁
            // 判断是否是自己的锁
            // 刚判断完 还没来及删除 就释放锁，别人刚获取锁  接着就删除锁  把别人的锁给删除了
            // 判断和删除不具备原子性
            // if (redis.call('get', KEYS[1]) == ARGV[1]) then return redis.call('del',KEYS[1]) else  return 0 end" 1 lock '2323-232-3-232
            String script = "if (redis.call('get', KEYS[1]) == ARGV[1]) then return redis.call('del',KEYS[1]) else  return 0 end";
            this.redisTemplate.execute(new DefaultRedisScript<>(script,Boolean.class), Arrays.asList("lock",uuid));

//            if (StringUtils.equals(uuid,this.redisTemplate.opsForValue().get("lock"))) {
//                this.redisTemplate.delete("lock");
//            }
        }

    }


    public void testRead() {
        RReadWriteLock rwLock = this.redissonClient.getReadWriteLock("rwLock");
        rwLock.readLock().lock(10,TimeUnit.SECONDS);
    }

    public void testWrite() {
        RReadWriteLock rwLock = this.redissonClient.getReadWriteLock("rwLock");
        rwLock.writeLock().lock(10,TimeUnit.SECONDS);
    }

    public void testLatch() {
        try {
            RCountDownLatch latch = this.redissonClient.getCountDownLatch("latch");
            latch.trySetCount(6);
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testCountDown() {
        RCountDownLatch latch = this.redissonClient.getCountDownLatch("latch");
        latch.countDown();
    }
}

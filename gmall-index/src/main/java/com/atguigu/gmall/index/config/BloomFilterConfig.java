package com.atguigu.gmall.index.config;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.index.feign.GmallPmsClient;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Configuration
public class BloomFilterConfig {


    @Autowired
    public RedissonClient redissonClient;

    @Autowired
    private GmallPmsClient pmsClient;

    private static final String KEY_PREFIX = "index:cates:[";

    @Bean
    public RBloomFilter bloomFilter() {
        RBloomFilter<Object> bloomFilter = this.redissonClient.getBloomFilter("index:bloom");
        // 误判率0.03
        bloomFilter.tryInit(1000,0.03);
        // 查询一级分类
        ResponseVo<List<CategoryEntity>> catesResponseVo = this.pmsClient.queryCategoriesById(0l);
        // 查询一级分类信息
        List<CategoryEntity> categoryEntities = catesResponseVo.getData();
        if(!CollectionUtils.isEmpty(categoryEntities)){
            categoryEntities.forEach(categoryEntity -> {
                // 添加元素
                bloomFilter.add(KEY_PREFIX +categoryEntity.getId() + "]");
            });
        }
        return bloomFilter;
    }

}

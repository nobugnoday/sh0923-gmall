package com.atguigu.gmall.index.aspect;


import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GmallCache {


    /**
     *  缓存key 的前缀
     * @return
     */
    String prefix() default "";


    /**
     *  缓存的过期时间 单位min
     * @return
     */
    int timeout() default 1440;


    /**
     * 缓存的雪崩  给缓存时间添加随机范围 单位 min
     * 50
     * @return
     */
    int random() default 50;

    /**
     *  为了防止缓存击穿 添加分布式锁的前缀
     *
     * @return
     */
    String lock() default "lock:";

}



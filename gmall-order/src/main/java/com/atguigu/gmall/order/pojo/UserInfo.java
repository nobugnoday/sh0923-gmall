package com.atguigu.gmall.order.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserInfo {

    private Long userId;
    private String userKey;
}

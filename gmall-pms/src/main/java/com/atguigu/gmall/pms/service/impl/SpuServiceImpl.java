package com.atguigu.gmall.pms.service.impl;

import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.pms.feign.GmallSmsClient;
import com.atguigu.gmall.pms.mapper.SkuMapper;
import com.atguigu.gmall.pms.service.*;
import com.atguigu.gmall.pms.vo.SkuVo;
import com.atguigu.gmall.pms.vo.SpuAttrValueVo;
import com.atguigu.gmall.pms.vo.SpuVo;
import com.atguigu.gmall.sms.vo.SkuSaveVo;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.mapper.SpuMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("spuService")
public class SpuServiceImpl extends ServiceImpl<SpuMapper, SpuEntity> implements SpuService {

    @Autowired
    private SpuDescService descService;

    @Autowired
    private SpuAttrValueService spuAttrValueService;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private SkuImagesService imagesService;

    @Autowired
    private SkuAttrValueService saleAttrService;

    @Autowired
    private GmallSmsClient smsClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<SpuEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<SpuEntity>()
        );

        return new PageResultVo(page);
    }

    @Override
    public PageResultVo querySpuByCidOrKeyPage(PageParamVo paramVo, Long cid) {
        QueryWrapper<SpuEntity> wrapper = new QueryWrapper<>();
        // 判断cid 不为空 或者不为0  根据分类查询
        if (cid != 0) {
            wrapper.eq("category_id",cid);
        }
        //  如果关键字不为空 ，拼接查询条件
        // SELECT * FROM pms_spu WHERE category_id = 225 AND (id = 7 OR 'name' LIKE '%7%');
        String key = paramVo.getKey();
        if (StringUtils.isNotBlank(key)) {
            //  add 后需要小括号，所以此处使用add(Consumer)
            wrapper.and(t -> t.eq("id",key).or().like("name",key));
        }
        IPage<SpuEntity> page = this.page(
                paramVo.getPage(),
              wrapper
        );

        return new PageResultVo(page);
    }

    @Override
    @GlobalTransactional
    public void bigSave(SpuVo spu) {
        // 1 包存spu的 相关信息
        // 1.1 保存spu基本信息 spu_info
        Long spuId = saveSpuInfo(spu);

        // 1.2 保存spu的描述信息 spu_desc
//        this.saveSpuDesc(spu, spuId);
        this.descService.saveSpuDesc(spu,spuId);

//        int i = 1 / 0;
//        try {
//            TimeUnit.SECONDS.sleep(4);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        // 1.3 保存pms_spu_attr_value  规格参数表
        SaveBaseAttr(spu, spuId);

        // 2 保存sku的相关信息
        saveSkuInfo(spu, spuId);

//        int i  = 10 /0;
        this.rabbitTemplate.convertAndSend("PMS_ITEM_EXCHANGE","item.insert",spuId);
    }

    private void saveSkuInfo(SpuVo spu, Long spuId) {
        List<SkuVo> skus = spu.getSkus();
        if (CollectionUtils.isEmpty(skus)) {
            return;
        }

        skus.forEach(skuVo -> {

            // 2.1 保存pms_sku
            // 设置sku所属的id
            skuVo.setSpuId(spuId);
            // 设置sku中分类的id  和 品牌的id
            skuVo.setCategoryId(spu.getCategoryId());
            skuVo.setBrandId(spu.getBrandId());
            // 设置默认图片信息
            List<String> images = spu.getSpuImages();
            System.out.println("images = " + images);
            if (!CollectionUtils.isEmpty(images)){
                // 进行判断 是否是第一张默认的图片信息
                skuVo.setDefaultImage(StringUtils.isBlank(skuVo.getDefaultImage())? images.get(0) : skuVo.getDefaultImage()  );
            }
            this.skuMapper.insert(skuVo);
            Long skuId = skuVo.getId();

            // 2.2 保存pms_sku_image   保存图片表
            if (!CollectionUtils.isEmpty(images)) {
                this.imagesService.saveBatch(  images.stream().map(image -> {
                    SkuImagesEntity imagesEntity = new SkuImagesEntity();
                    imagesEntity.setSkuId(skuId);
                    imagesEntity.setUrl(image);
                    imagesEntity.setDefaultStatus(StringUtils.equals(skuVo.getDefaultImage(),image)?1 : 0);
                    return imagesEntity;
                }).collect(Collectors.toList()));
            }
            // 2.3 保存pms_sku_attr_value  销售类型规格参数表
            List<SkuAttrValueEntity> saleAttrs = skuVo.getSaleAttrs();
            saleAttrs.forEach(saleAttr -> saleAttr.setSkuId(skuId));
            this.saleAttrService.saveBatch(saleAttrs);
            // 3 保存营销相关信息
            SkuSaveVo skuSaveVo = new SkuSaveVo();
            BeanUtils.copyProperties(skuVo,skuSaveVo);
            skuSaveVo.setSkuId(skuId);
            this.smsClient.saveSales(skuSaveVo);

        });
    }

    private void SaveBaseAttr(SpuVo spu, Long spuId) {
        List<SpuAttrValueVo> baseAttrs = spu.getBaseAttrs();
        if (!CollectionUtils.isEmpty(baseAttrs)) {

            this.spuAttrValueService.saveBatch(baseAttrs.stream().map(spuAttrValueVo -> {
                SpuAttrValueEntity spuAttrValueEntity = new SpuAttrValueEntity();
                BeanUtils.copyProperties(spuAttrValueVo,spuAttrValueEntity);
                spuAttrValueEntity.setSpuId(spuId);
                return spuAttrValueEntity;
            }).collect(Collectors.toList()));
        }
    }



    private Long saveSpuInfo(SpuVo spu) {
        spu.setCreateTime(new Date());
        spu.setUpdateTime(spu.getCreateTime());
        this.save(spu);
        return spu.getId();
    }

//    public static void main(String[] args) {
//        List<User> users = Arrays.asList(
//                new User(11,"小小",20),
//                new User(11,"小吴",30),
//                new User(11,"小王",40),
//                new User(11,"小张",50),
//                new User(11,"小许",60)
//        );
//        // 过滤 转换  总和   转化map  总和reduce
//
//        System.out.println(users.stream().filter(user -> user.getAge() > 22).collect(Collectors.toList()));
//        System.out.println(users.stream().map(User::getName).collect(Collectors.toList()));
//
//        System.out.println(users.stream().map(user -> {
//            Student student = new Student();
//            student.setUsername(user.getName());
//            student.setAge(user.getAge());
//            return student;
//        }).collect(Collectors.toList()));
//
//        System.out.println(users.stream().map(User::getAge).reduce((a, b) -> a + b));
//
//    }
}
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//class User{
//    Integer id;
//    String name;
//    Integer age;
//}
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//class Student{
//    String username;
//    Integer age;
//}










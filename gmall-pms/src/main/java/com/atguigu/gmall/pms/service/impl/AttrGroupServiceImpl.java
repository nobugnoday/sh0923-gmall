package com.atguigu.gmall.pms.service.impl;

import com.atguigu.gmall.pms.entity.AttrEntity;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.entity.SpuAttrValueEntity;
import com.atguigu.gmall.pms.mapper.AttrMapper;
import com.atguigu.gmall.pms.mapper.SkuAttrValueMapper;
import com.atguigu.gmall.pms.mapper.SpuAttrValueMapper;
import com.atguigu.gmall.pms.vo.AttrValuesVo;
import com.atguigu.gmall.pms.vo.ItemGroupVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.mapper.AttrGroupMapper;
import com.atguigu.gmall.pms.entity.AttrGroupEntity;
import com.atguigu.gmall.pms.service.AttrGroupService;
import org.springframework.util.CollectionUtils;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupMapper, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private AttrMapper attrMapper;

    @Autowired
    private SpuAttrValueMapper spuAttrValueMapper;

    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;

    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<AttrGroupEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageResultVo(page);
    }

        @Override
        public List<AttrGroupEntity> queryGroupswithAttrsByCid(Long cid) {
            // 1 根据分类id查询分组
            List<AttrGroupEntity> groupEntities = this.list(new QueryWrapper<AttrGroupEntity>().eq("category_id",cid));

            // 判断查询的分组是否为0
            if (CollectionUtils.isEmpty(groupEntities)) {
                return null;
            }

        // 2 遍历所有分组，根据分组的id查询分组下的规则参数
        groupEntities.forEach(group ->{
           List<AttrEntity> attrEntities =  this.attrMapper.selectList(new QueryWrapper<AttrEntity>().eq("group_id",group.getId()).eq("type",1));
           group.setAttrEntities(attrEntities);

        });
        return groupEntities;
    }

    @Override
    public List<ItemGroupVo> queryGroupWithAttrValuesByCidAndSpuIdAndSkuId(Long cid, Long spuId, Long skuId) {
        // 1 根据分类id查询分组

        List<AttrGroupEntity> groupEntities = this.list(new QueryWrapper<AttrGroupEntity>().eq("category_id",cid));
        if (CollectionUtils.isEmpty(groupEntities)){
            return null;
        }

        return groupEntities.stream().map(group ->{
            ItemGroupVo groupVo = new ItemGroupVo();
            groupVo.setId(group.getId());
            groupVo.setName(group.getName());
            //2 遍历分组， 查询每个分组下的规格参数
            List<AttrEntity> attrEntities = this.attrMapper.selectList(new QueryWrapper<AttrEntity>().eq("group_id",group.getId()));
            if (CollectionUtils.isEmpty(attrEntities)) {
                return groupVo;
            }

            List<Long> attrIds = attrEntities.stream().map(AttrEntity::getId).collect(Collectors.toList());
            List<AttrValuesVo> attrValuesVos = new ArrayList<>();
            // 3 查询基本属性及值
            List<SpuAttrValueEntity> baseAttrValues = this.spuAttrValueMapper.selectList(new QueryWrapper<SpuAttrValueEntity>().in("attr_id", attrIds).eq("spu_id", spuId));
            if (!CollectionUtils.isEmpty(baseAttrValues)) {

                attrValuesVos.addAll(baseAttrValues.stream().map(spuAttrValueEntity -> {
                    AttrValuesVo attrValuesVo = new AttrValuesVo();
                    BeanUtils.copyProperties(spuAttrValueEntity,attrValuesVo);
                    return attrValuesVo;
                }).collect(Collectors.toList()));
            }


            // 4 查询销售属性及值
            List<SkuAttrValueEntity> saleAttrValues = this.skuAttrValueMapper.selectList(new QueryWrapper<SkuAttrValueEntity>().in("attr_id", attrIds).eq("sku_id", skuId));
            if (!CollectionUtils.isEmpty(saleAttrValues)){

                attrValuesVos.addAll(saleAttrValues.stream().map(skuAttrValueEntity -> {
                    AttrValuesVo attrValuesVo = new AttrValuesVo();
                    BeanUtils.copyProperties(skuAttrValueEntity,attrValuesVo);
                    return attrValuesVo;
                }).collect(Collectors.toList()));
            }

            groupVo.setAttrValues(attrValuesVos);
            return groupVo;
        }).collect(Collectors.toList());


    }

}
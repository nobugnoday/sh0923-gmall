package com.atguigu.gmall.pms.service;

import com.atguigu.gmall.pms.vo.SaleAttrValuesVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-08 19:37:54
 */
public interface SkuAttrValueService extends IService<SkuAttrValueEntity> {

    PageResultVo queryPage(PageParamVo paramVo);

    List<SkuAttrValueEntity> querySearchAttrValueBySkuId(Long cid, Long skuId);

    List<SaleAttrValuesVo> querySalesAttrsBySpuId(Long spuId);

    Map<String, Object> querySaleAttrsMappingSkuIdBySpuId(Long spuId);
}


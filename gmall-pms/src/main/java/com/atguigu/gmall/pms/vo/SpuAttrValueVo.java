package com.atguigu.gmall.pms.vo;


import com.atguigu.gmall.pms.entity.SpuAttrValueEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class SpuAttrValueVo extends SpuAttrValueEntity {

    // 如果接受的集合为空，则不设置
    public void setValueSelected(List<String> valueSelected) {
        if (CollectionUtils.isEmpty(valueSelected)) {
            return;
        }
        //将请求中的valueSelected  转化成数组
        this.setAttrValue(StringUtils.join(valueSelected,","));
    }
}

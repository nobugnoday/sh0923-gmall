package com.atguigu.gmall.pms.mapper;

import com.atguigu.gmall.pms.entity.SpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-08 19:37:54
 */
@Mapper
public interface SpuMapper extends BaseMapper<SpuEntity> {
	
}

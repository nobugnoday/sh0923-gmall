package com.atguigu.gmall.pms.service.impl;

import com.atguigu.gmall.pms.entity.AttrEntity;
import com.atguigu.gmall.pms.entity.SkuEntity;
import com.atguigu.gmall.pms.mapper.AttrMapper;
import com.atguigu.gmall.pms.mapper.SkuMapper;
import com.atguigu.gmall.pms.vo.SaleAttrValuesVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.common.bean.PageParamVo;

import com.atguigu.gmall.pms.mapper.SkuAttrValueMapper;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.service.SkuAttrValueService;
import org.springframework.util.CollectionUtils;


@Service("skuAttrValueService")
public class SkuAttrValueServiceImpl extends ServiceImpl<SkuAttrValueMapper, SkuAttrValueEntity> implements SkuAttrValueService {

    @Autowired
    private AttrMapper attrMapper;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private SkuAttrValueMapper attrValueMapper;


    @Override
    public PageResultVo queryPage(PageParamVo paramVo) {
        IPage<SkuAttrValueEntity> page = this.page(
                paramVo.getPage(),
                new QueryWrapper<SkuAttrValueEntity>()
        );

        return new PageResultVo(page);
    }

    @Override
    public List<SkuAttrValueEntity> querySearchAttrValueBySkuId(Long cid, Long skuId) {
       // 1 查询出检索类型的规格参数
        List<AttrEntity> attrEntities =  this.attrMapper.selectList(new QueryWrapper<AttrEntity>().eq("category_id",cid).eq("search_type",1));
        if (CollectionUtils.isEmpty(attrEntities)){
            return  null;
        }
        List<Long>  attrIds =  attrEntities.stream().map(AttrEntity::getId).collect(Collectors.toList());
        // 2  查询销售类型的规格参数及值
        return this.list(new QueryWrapper<SkuAttrValueEntity>().eq("sku_id",skuId).in("attr_id",attrIds));
    }


    // 根据spuId  查询所有sku的营销属性
    @Override
    public List<SaleAttrValuesVo> querySalesAttrsBySpuId(Long spuId) {
        List<Long> skuIds = getSkuIdsBySpuId(spuId);
        if (CollectionUtils.isEmpty(skuIds)) return null;


        // 2 查询所有sku的销售属性   根据id进行降序排序
        List<SkuAttrValueEntity> skuAttrValueEntities = this.list(new QueryWrapper<SkuAttrValueEntity>().in("sku_id", skuIds).orderByAsc("attr_id"));
        // 以规格参数的id分组
        List<SaleAttrValuesVo> saleAttrValuesVos = new ArrayList<>();
//        Map<Long, List<SkuAttrValueEntity>> map = skuAttrValueEntities.stream().collect(Collectors.groupingBy(SkuAttrValueEntity::getAttrId));
        Map<Long, List<SkuAttrValueEntity>> map = skuAttrValueEntities.stream().collect(Collectors.groupingBy(SkuAttrValueEntity::getAttrId));
        map.forEach((attrId,attrValueEntities) -> {
            SaleAttrValuesVo saleAttrValuesVo = new SaleAttrValuesVo();
            saleAttrValuesVo.setAttrId(attrId);
            // 这里只有有该分组，该分组下必然至少会有一个元素
            saleAttrValuesVo.setAttrName(attrValueEntities.get(0).getAttrName());

//            saleAttrValuesVo.setAttrValues(attrValueEntities.stream().map(SkuAttrValueEntity::getAttrValue).collect(Collectors.toSet()));

            saleAttrValuesVo.setAttrValues(attrValueEntities.stream().map(SkuAttrValueEntity::getAttrValue).collect(Collectors.toSet()));
            saleAttrValuesVos.add(saleAttrValuesVo);
        });
        return saleAttrValuesVos;
    }

    private List<Long> getSkuIdsBySpuId(Long spuId) {
        // 1 查询spu下所有的sku 集合
        List<SkuEntity> skuEntities = this.skuMapper.selectList(new QueryWrapper<SkuEntity>().eq("spu_id", spuId));
        if (CollectionUtils.isEmpty(skuEntities)) {
            return null;
        }
        // 获取skuId 集合
        List<Long> skuIds = skuEntities.stream().map(SkuEntity::getId).collect(Collectors.toList());
        return skuIds;
    }

    @Override
    public Map<String, Object> querySaleAttrsMappingSkuIdBySpuId(Long spuId) {
        List<Long> skuIds = this.getSkuIdsBySpuId(spuId);
        if (CollectionUtils.isEmpty(skuIds)) return null;

        List<Map<String, Object>> maps = this.attrValueMapper.querySalesAttrsMappingSkuIdBySpuIds(skuIds);
        return maps.stream().collect(Collectors.toMap(map -> map.get("attrValues").toString(), map -> map.get("sku_id")));
    }
}
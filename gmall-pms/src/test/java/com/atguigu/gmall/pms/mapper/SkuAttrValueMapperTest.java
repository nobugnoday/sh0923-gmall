package com.atguigu.gmall.pms.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class SkuAttrValueMapperTest {
    @Autowired
    public SkuAttrValueMapper skuAttrValueMapper;

    @Test
    void querySalesAttrsMappingSkuIdBySpuIds() {
        System.out.println(this.skuAttrValueMapper.querySalesAttrsMappingSkuIdBySpuIds(Arrays.asList(92l, 93l, 94l, 95l)));
    }
}
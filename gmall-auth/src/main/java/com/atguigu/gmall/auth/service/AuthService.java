package com.atguigu.gmall.auth.service;

import com.atguigu.gmall.auth.config.JwtProperties;
import com.atguigu.gmall.auth.feign.GmallUmsClient;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.common.exception.UserException;
import com.atguigu.gmall.common.utils.CookieUtils;
import com.atguigu.gmall.common.utils.IpUtils;
import com.atguigu.gmall.common.utils.JwtUtils;
import com.atguigu.gmall.ums.entity.UserEntity;
import io.jsonwebtoken.Jwt;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@EnableConfigurationProperties(JwtProperties.class)
@Service
public class AuthService {

    @Autowired
    private GmallUmsClient gmallUmsClient;

    @Autowired
    private JwtProperties properties;

    public void login(String loginName, String password, HttpServletRequest request, HttpServletResponse response) {
        try {
            // 1 调用远程接口查询登录名和密码是否正确
            ResponseVo<UserEntity> userEntityResponseVo = this.gmallUmsClient.queryUser(loginName, password);
            UserEntity userEntity = userEntityResponseVo.getData();
            // 2 判断用户信息是否为空
            if (userEntity == null) {
                throw new UserException("用户名或者密码错误");
            }

            // 3 组装载荷信息 ，用户名id 、用户名、ip(防盗用)
            Map<String, Object> map = new HashMap<>();
            map.put("id",userEntity.getId());
            map.put("username",userEntity.getUsername());
            map.put("ip", IpUtils.getIpAddressAtService(request));

            // 4 生成 Jwt
            String token = JwtUtils.generateToken(map, this.properties.getPrivateKey(), this.properties.getExpire());

            // 5 把jwt类型的token放入cookie中
            CookieUtils.setCookie(request,response,this.properties.getCookieName(),token,this.properties.getExpire() * 60);
            // 6.把用户的昵称放入cookie中
            CookieUtils.setCookie(request,response,this.properties.getUnick(),userEntity.getNickname(),this.properties.getExpire() * 60);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void register(UserEntity userEntity, String code) {
//        // 1 检验验证码
//        // 2 生成盐
//        String salt = StringUtils.replace(UUID.randomUUID().toString(), "-", "");
//        userEntity.setSalt(salt);
//        // 3 对密码进行加密
//        userEntity.setPassword(DigestUtils.md5Hex(salt + DigestUtils.md5Hex(userEntity.getPassword())));
//        // 4 设置创建时间。。。
//        userEntity.setCreateTime(new Date());  // 时间
//        userEntity.setLevelId(1l);   // 等级
//        userEntity.setStatus(1);  // '状态',
//        userEntity.setIntegration(0);  // 购物积分
//        userEntity.setGrowth(0);  // 赠送积分
//        userEntity.setNickname(userEntity.getUsername());   // 昵称
//
//        // 写入数据库
////        boolean b = this.save(userEntity);
//        // 5 删除Redis中的验证码
//    }

    public void register(UserEntity userEntity) {
        // 1 检验验证码
        // 2 生成盐
        String salt = StringUtils.replace(UUID.randomUUID().toString(), "-", "");
        userEntity.setSalt(salt);
        // 3 对密码进行加密
        userEntity.setPassword(DigestUtils.md5Hex(salt + DigestUtils.md5Hex(userEntity.getPassword())));
        // 4 设置创建时间。。。
        userEntity.setCreateTime(new Date());  // 时间
        userEntity.setLevelId(1l);   // 等级
        userEntity.setStatus(1);  // '状态',
        userEntity.setIntegration(0);  // 购物积分
        userEntity.setGrowth(0);  // 赠送积分
        userEntity.setNickname(userEntity.getUsername());   // 昵称

        // 写入数据库
//        boolean b = this.save(userEntity);
        // 5 删除Redis中的验证码
    }

}

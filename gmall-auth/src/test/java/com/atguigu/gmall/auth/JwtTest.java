package com.atguigu.gmall.auth;

import com.atguigu.gmall.common.utils.JwtUtils;
import com.atguigu.gmall.common.utils.RsaUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class JwtTest {

    // 别忘了创建D:\\project\rsa目录
	private static final String pubKeyPath = "D:\\JavaEE_Second\\stage8_commerce\\project-0923\\rsa\\rsa.pub";
    private static final String priKeyPath = "D:\\JavaEE_Second\\stage8_commerce\\project-0923\\rsa\\rsa.pri";

    private PublicKey publicKey;

    private PrivateKey privateKey;

    @Test
    public void testRsa() throws Exception {
        RsaUtils.generateKey(pubKeyPath, priKeyPath, "234");
    }

    @BeforeEach
    public void testGetRsa() throws Exception {
        this.publicKey = RsaUtils.getPublicKey(pubKeyPath);
        this.privateKey = RsaUtils.getPrivateKey(priKeyPath);
    }

    @Test
    public void testGenerateToken() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("id", "11");
        map.put("username", "liuyan");
        // 生成token
        String token = JwtUtils.generateToken(map, privateKey, 2);
        System.out.println("token = " + token);
    }

    @Test
    public void testParseToken() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6IjExIiwidXNlcm5hbWUiOiJsaXV5YW4iLCJleHAiOjE2MTY2Nzk2MzF9.U5Ekd65IwFlre2TEjTeIebprjj_mIzLEne7Pxi0tni5ts7ntP5HdElX7oTGV0JFDtEN8UNHXG5rVoGPQjN_KVGaUZtzvyZ8gHCk7irlbvqf5zrtBA7NxlS1_PTN1WFiGr0FIjzpdqYzkpdkbMtjoXCJ-0DtAtwFMRwm7YHgO7zlbY5Ku5ESbsvuixcjrNwfV8WMa-cD0e4e3aJdmZAdoej4prtl1UD7u63Kv_d45ohP7Umi8TWlIfqUuYu-fcnxnckSLz5_YBztXFACGV6ktiw4rMEctYrEo7sBQX2J-33fCpIMHV4thXehIibAJ8aP5zNgPOvL559i7OvmsDu7kww";

        // 解析token
        Map<String, Object> map = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + map.get("id"));
        System.out.println("userName: " + map.get("username"));
    }
}
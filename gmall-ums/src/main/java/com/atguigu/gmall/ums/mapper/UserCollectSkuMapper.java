package com.atguigu.gmall.ums.mapper;

import com.atguigu.gmall.ums.entity.UserCollectSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 关注商品表
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-09 00:25:02
 */
@Mapper
public interface UserCollectSkuMapper extends BaseMapper<UserCollectSkuEntity> {
	
}

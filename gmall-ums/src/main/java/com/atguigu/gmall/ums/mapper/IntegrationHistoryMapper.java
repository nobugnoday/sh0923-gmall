package com.atguigu.gmall.ums.mapper;

import com.atguigu.gmall.ums.entity.IntegrationHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 购物积分记录表
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-09 00:25:02
 */
@Mapper
public interface IntegrationHistoryMapper extends BaseMapper<IntegrationHistoryEntity> {
	
}

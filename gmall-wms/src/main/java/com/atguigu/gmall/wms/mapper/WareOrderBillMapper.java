package com.atguigu.gmall.wms.mapper;

import com.atguigu.gmall.wms.entity.WareOrderBillEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-09 08:38:47
 */
@Mapper
public interface WareOrderBillMapper extends BaseMapper<WareOrderBillEntity> {
	
}

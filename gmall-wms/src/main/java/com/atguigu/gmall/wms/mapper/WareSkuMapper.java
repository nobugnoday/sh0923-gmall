package com.atguigu.gmall.wms.mapper;

import com.atguigu.gmall.wms.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品库存
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-09 08:38:47
 */
@Mapper
public interface WareSkuMapper extends BaseMapper<WareSkuEntity> {

    // 定义一个验库存的方法
    List<WareSkuEntity> check(@Param("skuId")Long skuId,@Param("count")Integer count);

    // 锁库存
    Integer lockStock(@Param("id")Long id,@Param("count")Integer count);

    // 解库存
    Integer unlock(@Param("id")Long id, @Param("count")Integer count);
    // 减库存
    Integer minus(@Param("id")Long id, @Param("count")Integer count);

}

package com.atguigu.gmall.wms.mapper;

import com.atguigu.gmall.wms.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-09 08:38:48
 */
@Mapper
public interface PurchaseMapper extends BaseMapper<PurchaseEntity> {
	
}

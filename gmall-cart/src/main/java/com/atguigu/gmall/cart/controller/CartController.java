package com.atguigu.gmall.cart.controller;

import com.atguigu.gmall.cart.interceptor.LoginInterceptor;
import com.atguigu.gmall.cart.pojo.Cart;
import com.atguigu.gmall.cart.server.CartServer;

import com.atguigu.gmall.common.bean.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Controller
public class CartController {

    @Autowired
    private CartServer cartServer;

    // 1 新增购物车
    @GetMapping
    public String saveCart(Cart cart){
        this.cartServer.saveCart(cart);
        return "redirect:http://cart.gmall.com/addCart.html?skuId=" + cart.getSkuId() + "&count=" + cart.getCount();
    }

    @GetMapping("addCart.html")
    public String queryCartBySkuId(@RequestParam("skuId")Long skuId, @RequestParam("count")Integer count, Model model){
        Cart cart = this.cartServer.queryCartBySkuId(skuId,count);
        model.addAttribute("cart",cart);

        return "addCart";
    }


    @GetMapping("cart.html")
    public String queryCarts(Model model){
        List<Cart> carts = this.cartServer.queryCarts();
        model.addAttribute("carts",carts);
        return "cart";
    }


    @PostMapping("updateNum")
    @ResponseBody
    public ResponseVo updateNum(@RequestBody Cart cart){

        this.cartServer.updateNum(cart);
        return ResponseVo.ok();
    }

    @PostMapping("deleteCart")
    @ResponseBody
    public ResponseVo deleteCart(@RequestParam("skuId")Long skuId){
        this.cartServer.deleteCart(skuId);
        return ResponseVo.ok();

    }

    // 据用户id查询该用户选中的购物车记录（skuId count）
    @GetMapping("user/{userId}")
    @ResponseBody
    public ResponseVo<List<Cart>> queryCheckedCarts (@PathVariable("userId")Long userId){

        List<Cart> carts = this.cartServer.queryCheckedCarts(userId);
        return ResponseVo.ok(carts);

    }



    @GetMapping("test")
    @ResponseBody
    public String test(HttpServletRequest request){
//        System.out.println(" 这是controller 的测试方法 " + LoginInterceptor.userInfo);

//        System.out.println(request.getAttribute("userId" )+ "=========" + request.getAttribute("userKey"));
//        System.out.println(LoginInterceptor.getUserInfo());
        long now = System.currentTimeMillis();
        System.out.println("controller 异步开始......");
         this.cartServer.executtor1();
        this.cartServer.executtor2();
//        future1.addCallback(result -> System.out.println("调用成功future1" +future1),ex ->System.out.println("调用成功future1" +ex.getMessage()));
//        future2.addCallback(result -> System.out.println("调用成功future1" +future2),ex ->System.out.println("调用成功future2" +ex.getMessage()));

//        try {
//            System.out.println( "获取子任务的结果集" + future1.get());
//            System.out.println( "获取子任务的结果集" + future2.get());
//        } catch (Exception e) {
//            System.out.println("捕获的异常信息" + e.getMessage());
//        }
        System.out.println("controller 异结束......" + (System.currentTimeMillis() - now));
        return "hello test";
    }
}

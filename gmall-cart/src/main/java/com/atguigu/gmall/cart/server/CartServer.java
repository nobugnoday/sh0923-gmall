package com.atguigu.gmall.cart.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.cart.feign.GmallPmsClient;
import com.atguigu.gmall.cart.feign.GmallSmsClient;
import com.atguigu.gmall.cart.feign.GmallWmsClient;
import com.atguigu.gmall.cart.interceptor.LoginInterceptor;
import com.atguigu.gmall.cart.mapper.CartMapper;
import com.atguigu.gmall.cart.pojo.Cart;
import com.atguigu.gmall.cart.pojo.UserInfo;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.common.exception.CartException;
import com.atguigu.gmall.pms.api.GmallPmsApi;
import com.atguigu.gmall.pms.entity.SkuAttrValueEntity;
import com.atguigu.gmall.pms.entity.SkuEntity;
import com.atguigu.gmall.sms.api.GmallSmsApi;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import com.atguigu.gmall.wms.api.GmallWmsApi;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.sql.Struct;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class CartServer {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private CartAsyncServer cartAsyncServer;

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private GmallWmsClient wmsClient;

    @Autowired
    private GmallSmsClient smsClient;

    private static final  String KEY_PREFIX="cart:info:";

    private static final  String PRICE_PREFIX="cart.price:";
    // 新增购物车
    public void saveCart(Cart cart) {
        // 1 获取登录状态 未登录-userKey  登录-userId
        String userId = getUserId();
        String key = KEY_PREFIX + userId;
        // 2 判断当前用户的购物车中是否包含该记录 : redisTemplate.opsForHash()
        // 内层map<skuId,cart>
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(key);

        // 查询商品的数量
        BigDecimal count = cart.getCount();
        // 获取cart的key
        String skuIdString = cart.getSkuId().toString();
        if (hashOps.hasKey(skuIdString)){
            // 包含 ，更新数量
            // 根据skuIdString 查询cart
            String json = hashOps.get(skuIdString).toString();
            // 将json转换为cart
            cart = JSONObject.parseObject(json,Cart.class);
            // 数量的累加
            cart.setCount(cart.getCount().add(count));
            // 把更新之后的cart 写入redis 和 mysql

            // 保存到数据库
            this.cartAsyncServer.updateCart(userId,cart.getSkuId(),cart);

        } else {
            // 不包含，新增一条记录
            cart.setCheck(true);
            cart.setUserId(userId);

            // 查询sku 的信息
            ResponseVo<SkuEntity> skuEntityResponseVo = this.pmsClient.querySkuById(cart.getSkuId());
            SkuEntity skuEntity = skuEntityResponseVo.getData();
            if (skuEntity == null) {
                throw new CartException("亲 您加入购物车的商品不存在！！ ");
            }
            cart.setTitle(skuEntity.getTitle());
            cart.setPrice(skuEntity.getPrice());
            cart.setDefaultImage(skuEntity.getDefaultImage());

            // 库存
            ResponseVo<List<WareSkuEntity>> wareResponseVo = this.wmsClient.queryWareSkusBySkuId(cart.getSkuId());
            List<WareSkuEntity> wareSkuEntities = wareResponseVo.getData();
            if (!CollectionUtils.isEmpty(wareSkuEntities)){
                // 当库存大于 销量的时候 才能设计成功
                cart.setStore(wareSkuEntities.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
            }

            // 营销信息
            cart.setSales(null);
            ResponseVo<List<ItemSaleVo>> salesResponseVo = this.smsClient.querySalesBySkuId(cart.getSkuId());
            List<ItemSaleVo> itemSaleVos = salesResponseVo.getData();
            // 将itemSaleVos 转换为字符串 保存到数据库中
            cart.setSales(JSON.toJSONString(itemSaleVos));

            // 销售属性
            ResponseVo<List<SkuAttrValueEntity>> salesAttrsResponseVo = this.pmsClient.querySaleAttrValueBySkuId(cart.getSkuId());
            List<SkuAttrValueEntity> skuAttrValueEntities = salesAttrsResponseVo.getData();
            cart.setSaleAttrs(JSON.toJSONString(skuAttrValueEntities));

            // 新增到redis 和 mysql
            this.cartAsyncServer.insertCart(userId,cart);

            // 加入购物车 添加实时的价格缓存
            this.redisTemplate.opsForValue().set(PRICE_PREFIX + skuIdString,skuEntity.getPrice().toString());
        }


        hashOps.put(skuIdString,JSON.toJSONString(cart));
    }

    /**
     *  封装统一获取用户信息
     *  登录返回userId
     *  未登录返回userKey
     * @return
     */
    private String getUserId() {
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        String userId = "";
        // 外层的key
        if (userInfo.getUserId() != null) {
            userId = userInfo.getUserId().toString();
        } else {
            userId = userInfo.getUserKey();
        }
        return userId;
    }

    // 根据skuId  和 count查询购物车
    public Cart queryCartBySkuId(Long skuId, Integer count) {
        String userId = this.getUserId();
        // 获取内层的map操作对象
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        // 查看是否包含skuId
        if (hashOps.hasKey(skuId.toString())) {
            String json  = hashOps.get(skuId.toString()).toString();
            Cart cart = JSON.parseObject(json, Cart.class);
            cart.setCount(new BigDecimal(count));
            return cart;
        } else {
            throw new CartException("亲    当前用户不包含该购物车记录，");
        }

    }

    public List<Cart> queryCarts() {
        // 1 获取userKey 查询未登录的购物车
        UserInfo userInfo = LoginInterceptor.getUserInfo();
        String userKey = userInfo.getUserKey();
        // 获取所有登录的unLoginHashOps对象
        BoundHashOperations<String, Object, Object> unLoginHashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userKey);
        List<Object> unLoginCartJson = unLoginHashOps.values();
        List<Cart> unloginCarts = null;
        if (!CollectionUtils.isEmpty(unLoginCartJson)){

            unloginCarts = unLoginCartJson.stream().map(cartjson -> {
                Cart cart = JSON.parseObject(cartjson.toString(), Cart.class);
                cart.setCurrentPrice(new BigDecimal(this.redisTemplate.opsForValue().get(PRICE_PREFIX + cart.getSkuId())));
                return cart;
            }).collect(Collectors.toList());
        }

        //2 获取userId 判断是否为空，为空说明未登录 ， 直接返回未登录的购物车
        Long userId = userInfo.getUserId();
        if (userId == null) {
            return unloginCarts;
        }

        // 3 userId 不为空 ，合并未登录的购物车到登录状态的购物车
        BoundHashOperations<String, Object, Object> loginHashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        // 查看未登录的购物车是否为空
        if (!CollectionUtils.isEmpty(unloginCarts)){
            // 遍历未登录的购物车 ，合并到登录状态的购物车中
            unloginCarts.forEach(cart -> {
                // 获取skuId
                String skuIdString = cart.getSkuId().toString();
                // 获取数量
                BigDecimal count = cart.getCount();
                if(loginHashOps.hasKey(skuIdString)) {
                    // 如果登录状态的购物车已经包含了该记录 ，则进行累加数量
                    String cartJson = loginHashOps.get(skuIdString).toString();
                    // 将cartJson  转换成cart 实体类
                    cart = JSON.parseObject(cartJson,Cart.class);
                    cart.setCount(cart.getCount().add(count));
                    // 保存到redis  并异步保存到mysql中
                    // 将cart 集合转换成cartjson
//                    loginHashOps.put(skuIdString,JSON.toJSONString(cart));
                    this.cartAsyncServer.updateCart(userId.toString(),cart.getSkuId(),cart);
                } else {
                    // 不包含， 则新增一条记录
                    cart.setUserId(userId.toString());
                    // 保存到redis  并异步保存到mysql中
                    this.cartAsyncServer.insertCart(userId.toString(),cart);
                }
                loginHashOps.put(skuIdString, JSON.toJSONString(cart));
            });

            // 4 删除未登录的购物车
            this.redisTemplate.delete(KEY_PREFIX + userKey);
            this.cartAsyncServer.deleteCartByUserId(userKey);
        }

        // 5 查询登录状态的购物车并返回
        List<Object> loginCartJsons = loginHashOps.values();
        if (!CollectionUtils.isEmpty(loginCartJsons)){
            return loginCartJsons.stream().map(cartJson -> {
                Cart cart = JSON.parseObject(cartJson.toString(), Cart.class);
                cart.setCurrentPrice(new BigDecimal(this.redisTemplate.opsForValue().get(PRICE_PREFIX + cart.getSkuId())));
                return cart;
            }).collect(Collectors.toList());

        }
        return null;
    }


    public void updateNum(Cart cart) {
        // 获取userId
        String userId = this.getUserId();

        // 获取内层的map操作对象
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        if (hashOps.hasKey(cart.getSkuId().toString())){
            // 获取数量
            BigDecimal count = cart.getCount();
            String json = hashOps.get(cart.getSkuId().toString()).toString();
            cart = JSON.parseObject(json,Cart.class);
            cart.setCount(count);
            //跟新到数据库 redis mysql
            hashOps.put(cart.getSkuId().toString(),JSON.toJSONString(cart));
            this.cartAsyncServer.updateCart(userId,cart.getSkuId(),cart);
            return;
        }
        throw new  CartException("该用户的购物车不包含该记录");
    }


    @Async
    public String executtor1() {
        try {
            System.out.println("异步开始。。。。。。。。。。。。。。");
            TimeUnit.SECONDS.sleep(4);
            System.out.println("异步结束。。。。。。。。。。。。。。");
        } catch (InterruptedException e) {
//            return AsyncResult.forExecutionException(e);

        }
        return "executor1";
    }

    @Async
    public String executtor2() {
        try {
            System.out.println("异步开始。。。。。。。。。。。。。。");
            TimeUnit.SECONDS.sleep(5);
            int i = 1 / 0;
            System.out.println("异步结束。。。。。。。。。。。。。。");
        } catch (InterruptedException e) {
//            return AsyncResult.forExecutionException(e);
        }
        return "executor2";
    }


    public void deleteCart(Long skuId) {
        String userId = this.getUserId();

        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        hashOps.delete(skuId.toString());
        this.cartAsyncServer.deleteCartByUserIdAndSkuId(userId,skuId);
    }

    public List<Cart> queryCheckedCarts(Long userId) {
        BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
        List<Object> cartJsons = hashOps.values();
        if (!CollectionUtils.isEmpty(cartJsons)) {
            // 将CartJsons 转换为Cart 然后过滤选中的购物车
            return cartJsons.stream().map(cartJson -> JSON.parseObject(cartJson.toString(),Cart.class)).filter(Cart::getCheck).collect(Collectors.toList());
        }

        return null;
    }


//    @Async
//    public ListenableFuture<String> executtor1() {
//        try {
//            System.out.println("异步开始。。。。。。。。。。。。。。");
//            TimeUnit.SECONDS.sleep(4);
//            System.out.println("异步结束。。。。。。。。。。。。。。");
//        } catch (InterruptedException e) {
//            return AsyncResult.forExecutionException(e);
//
//        }
//        return AsyncResult.forValue("executor1");
//    }
//
//    @Async
//    public ListenableFuture<String> executtor2() {
//        try {
//            System.out.println("异步开始。。。。。。。。。。。。。。");
//            TimeUnit.SECONDS.sleep(5);
//            int i = 1 / 0;
//            System.out.println("异步结束。。。。。。。。。。。。。。");
//        } catch (InterruptedException e) {
//            return AsyncResult.forExecutionException(e);
//        }
//        return AsyncResult.forValue("executor2");
//    }



}

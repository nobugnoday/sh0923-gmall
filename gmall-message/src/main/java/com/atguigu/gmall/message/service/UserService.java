package com.atguigu.gmall.message.service;

import com.atguigu.gmall.ums.entity.UserEntity;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;
import org.apache.commons.lang.StringUtils;
import java.util.Date;
import java.util.UUID;

@Service
public class UserService {

    public void register(UserEntity userEntity, String code) {
        // 校验短信验证码
        // String cacheCode = this.redisTemplate.opsForValue().get(KEY_PREFIX + userEntity.getPhone());
        // if (!StringUtils.equals(code, cacheCode)) {
        //     return false;
        // }

        // 生成盐
        String salt = StringUtils.replace(UUID.randomUUID().toString(), "-", "");
        userEntity.setSalt(salt);

        // 对密码加密
        userEntity.setPassword(DigestUtils.md5Hex(salt + DigestUtils.md5Hex(userEntity.getPassword())));

        // 设置创建时间等
        userEntity.setCreateTime(new Date());
        userEntity.setLevelId(1l);
        userEntity.setStatus(1);
        userEntity.setIntegration(0);
        userEntity.setGrowth(0);
//        userEntity.setNickname(userEntity.getUserName());

        // 添加到数据库
//        boolean b = this.save(userEntity);

        // if(b){
        // 注册成功，删除redis中的记录
        // this.redisTemplate.delete(KEY_PREFIX + memberEntity.getPhone());
        // }
    }
}

package com.atguigu.gmall.message.controller;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.message.service.UserService;
import com.atguigu.gmall.ums.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("register")
    public ResponseVo<Object> register(UserEntity userEntity, @RequestParam("code") String code) {
        this.userService.register(userEntity, code);

        return ResponseVo.ok(null);
    }
}

package com.atguigu.gmall.wms.vo;

import lombok.Data;

@Data
public class SkuLockVo {

    private Long skuId;
    // 购买数量
    private Integer count;

    // 锁库存的状态
    private Boolean lock;

    // 在锁定成功的情况下，记录锁定成功的仓库id . 方便将来解锁
    private Long wareSkuId;

}

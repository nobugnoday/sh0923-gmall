package com.atguigu.gmall.oms.listener;

import com.atguigu.gmall.oms.entity.OrderEntity;
import com.atguigu.gmall.oms.mapper.OrderMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rabbitmq.client.Channel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class OrderListener {

    @Autowired
    private OrderMapper orderMapper;


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "ORDER_FAILURE_EXCHANGE",durable = "true"),
            exchange = @Exchange(value = "ORDER_EXCHANGE",ignoreDeclarationExceptions = "true",type = ExchangeTypes.TOPIC),
            key = {"order.failure"}
    ))
    public void disabledOrder(String orderToken, Channel channel, Message message) throws IOException {
        if (StringUtils.isBlank(orderToken)) {

            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            return;
        }


//        OrderEntity orderEntity = this.orderMapper.selectOne(new QueryWrapper<OrderEntity>().eq("order_sn", orderToken));
//        // 设置为无效订单
//        orderEntity.setStatus(5);

        // 标记为无效订单 ，注意初始状态
        this.orderMapper.updateStatus(orderToken,0,5);

        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }



    @RabbitListener(queues = {"ORDER_DEAD_QUEUE"})
    public void closeOrder(String orderToken, Channel channel, Message message) throws IOException {
        if (StringUtils.isBlank(orderToken)) {

            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            return;
        }

        // 关闭订单， 注意初始状态为0
        if (this.orderMapper.updateStatus(orderToken,0,4) == 1) {
            // 关单成功后 发送给wms  解锁
            this.rabbitTemplate.convertAndSend("ORDER_EXCHANGE","stock.unlock",orderToken);
        }

        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }



    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "ORDER_PAY_EXCHANGE",durable = "true"),
            exchange = @Exchange(value = "ORDER_EXCHANGE",ignoreDeclarationExceptions = "true",type = ExchangeTypes.TOPIC),
            key = {"order.pay"}
    ))
    public void payOrder(String orderToken, Channel channel, Message message) throws IOException {
        if (StringUtils.isBlank(orderToken)) {

            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            return;
        }


        // 更订单状态为待发货状态 ，注意初始状态 0
        if (this.orderMapper.updateStatus(orderToken,0,1 )==1) {
            // 如果订单状态更新成功，减库存
            this.rabbitTemplate.convertAndSend("ORDER.EXCHANGE","stock.minus",orderToken);
        }

        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }


}

package com.atguigu.gmall.oms.mapper;

import com.atguigu.gmall.oms.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 订单
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-08 23:59:48
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {
	// 根据orderToken  期望值 和 要修改的值
    Integer updateStatus(@Param("orderToken") String orderToken,@Param("expect") Integer expect,@Param("target") Integer target);
}

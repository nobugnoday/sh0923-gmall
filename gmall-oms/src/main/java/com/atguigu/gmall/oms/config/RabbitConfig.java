package com.atguigu.gmall.oms.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
public class RabbitConfig {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // @PostConstruct注解的方法会将在依赖注入完成之后被自动调用
    @PostConstruct
    public void init(){
        // 确认消息是否到达交换机 ， 不管消息是否到达交换机该方法都会执行
        this.rabbitTemplate.setConfirmCallback((correlationData, ack,  cause) ->{
            if (!ack){
                log.error("消息没有到达交换机：{}" ,cause);
            }
        });
        // 确认消息是否到达队列， 只有消息没有到达队列才执行该回调方法
        this.rabbitTemplate.setReturnCallback(( message,  replyCode,  replyText,  exchange,  routingKey) -> {
                log.error("消息没有到达交换机：{}，路由键：{}，消息内容：{}",exchange,routingKey,new String(message.getBody()));
        });
    }

}

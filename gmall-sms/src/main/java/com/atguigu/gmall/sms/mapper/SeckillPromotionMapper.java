package com.atguigu.gmall.sms.mapper;

import com.atguigu.gmall.sms.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author biaoge
 * @email biaoge@atguigu.com
 * @date 2021-03-08 20:35:52
 */
@Mapper
public interface SeckillPromotionMapper extends BaseMapper<SeckillPromotionEntity> {
	
}
